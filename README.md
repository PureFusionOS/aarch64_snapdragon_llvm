**To Build a standalone Chain:** 
	./make-standalone-toolchain-snapdragon-llvm.sh --ndk-dir=/home/<username>/Android/ndk --platform=android-26 --arch=arm64 --toolchain=aarch64-linux-android-<version string> --install-dir=/home/<username>/Android/Snapdragon_LLVM --use-llvm --system=linux-x86_64


	**Using Snapdragon LLVM ARM Compiler 3.8.8 with Android NDK:**

	The Snapdragon LLVM ARM Compiler 3.8.8 can be used as a drop-in replacement
	for LLVM shipped as part of the Android NDK.

	* 1. Snapdragon LLVM ARM Compiler 3.8.8 has been verified to work with Android
	* NDK versions r11b and r10e for Windows (64-bit) and Linux (64-bit)
	* 
	* 2. On Windows, it is assumed that the user has Cygwin setup as required by the
	* Android NDK instructions.
	* 
	* 3. On Windows and Linux it is recommended that you extract the Android NDK
	* under a directory which *does not* contain spaces, like:

	C:\android-ndk-r11b
	/local/mnt/workspace/android-ndk-r11b

	This directory is referred to as <NDK_ROOT> in this README.

	* 4. On Windows, extract Snapdragon-llvm-3.8.8-windows64.zip under the
	* <NDK_ROOT>. This contains 3 zip files:
	*  i.   Extract Snapdragon-llvm-3.8.8-toolchain.zip under <NDK_ROOT>
	*  ii.  If using Android NDK r11b, extract Snapdragon-llvm-3.8.8-setup-r11b.zip
	*  under <NDK_ROOT>
	*  iii. Else if using Android NDK r10e, extract
	*  Snapdragon-llvm-3.8.8-setup-r10e.zip under <NDK_ROOT>

	* 5. On Linux, extract Snapdragon-llvm-3.8.8-linux64.tar.gz under the
	* <NDK_ROOT>. This contains 3 tar files:
	*  i.   Extract Snapdragon-llvm-3.8.8-toolchain.tar.gz under <NDK_ROOT>
	*  ii.  If using Android NDK r11b, extract
	*  Snapdragon-llvm-3.8.8-setup-r11b.tar.gz under <NDK_ROOT>
	*  iii. Else if using Android NDK r10e, extract
	*  Snapdragon-llvm-3.8.8-setup-r10e.tar.gz under <NDK_ROOT>

	6. The following toolchains would be used for linking, by default:
	 For ARMv7 --> arm-linux-androideabi-4.9
	 For AArch64 --> aarch64-linux-android-4.9

	7. On Windows, in order to avoid errors due to missing MSVC Redistributable
	DLLs, make sure you set PATH to the Snapdragon LLVM bin directory as follows:
	export PATH=<NDK_ROOT>/toolchains/llvm-Snapdragon_LLVM_for_Android_3.8/
	prebuilt/windows-x86_64/bin:$PATH

	8. The Snapdragon LLVM ARM 3.8.8 plugin is designed to work with both 32-bit
	and 64-bit versions of the NDK as described below:

	For generating ARMv7 code, invoke your compilation line as follows:

	ndk-build NDK_TOOLCHAIN_VERSION=snapdragonclang3.8 APP_ABI="armeabi-v7a" -C
	<some_project>

	For generating AArch64 (64-bit) code, invoke your compilation line as follows:

	ndk-build NDK_TOOLCHAIN_VERSION=snapdragonclang3.8 APP_ABI="arm64-v8a" -C
	<some_project>

	9. If you want to specify your custom flags to the compiler in order to
	override the default flags you can use the variable APP_CFLAGS as follows:

	ndk-build NDK_TOOLCHAIN_VERSION=snapdragonclang3.8 APP_ABI="armeabi-v7a" \
	APP_CFLAGS="-O3" -C <some_project>

	Similarly, to specify custom flags for the linker you can use the
	flag APP_LDFLAGS.

	* 10. We STRONGLY RECOMMEND the following command line flags, to be set through
	* APP_CFLAGS, for best performance. These options ensure that all high
	* performance optimization features in the Snapdragon LLVM compiler are enabled
	* to deliver maximum performance in 32-bit and 64-bit modes. For this release, if
	* you continue to use the default Android NDK compatible flags, you may see
	* performance regression.
	* 
	*   ARMv7:
	* 	If your project does not require precise math, please set
	* 	APP_CFLAGS="-Ofast -mtune=krait"
	* 
	* 	If your project requires IEEE 754 floating point compliance, please set
	* 	APP_CFLAGS="-O3 -mtune=krait"
	* 
	*   AArch64:
	* 	If your project does not require precise math, please set
	* 	APP_CFLAGS="-Ofast"
	* 
	* 	If your project requires IEEE 754 floating point compliance, please set
	* 	APP_CFLAGS="-O3"

	**11. A standalone toolchain for the Android NDK environment using the Snapdragon
	LLVM ARM compiler can be created using the make-standalone-toolchain utility.
	Note: For NDK r11b, the default make-standalone-toolchain.sh cannot be used
	with Snapdragon LLVM toolchain to create a standalone toolchain. So we provide
	a custom script called make-standalone-toolchain-snapdragon-llvm.sh.

	For example, to create a standalone toolchain for Linux 64-bit environment, the
	following commands can be used:**

	For ARMv7:

	If using Android NDK r11b:
	<NDK_ROOT>/build/tools/make-standalone-toolchain-snapdragon-llvm.sh \
	--ndk-dir=<NDK_ROOT> --platform=android-19 --arch=arm \
	--install-dir=<some_dir> --use-llvm --system=linux-x86_64

	If using Android NDK r10e:
	<NDK_ROOT>/build/tools/make-standalone-toolchain.sh --ndk-dir=<NDK_ROOT> \
	--platform=android-19 --arch=arm --install-dir=<some_dir> \
	--toolchain=arm-linux-androideabi-4.9 \
	--llvm-version=Snapdragon_LLVM_for_Android_3.8 --system=linux-x86_64

	The above command lines specify that android-19 platform directory must be
	copied to be used as the standalone sysroot and GCC-4.9 toolchain must be
	copied to be used as the linker and for other binutils such as objdump.

	For AArch64:

	If using Android NDK r11b:
	<NDK_ROOT>/build/tools/make-standalone-toolchain-snapdragon-llvm.sh \
	--ndk-dir=<NDK_ROOT> --platform=android-21 --arch=arm64 \
	--install-dir=<some_dir> --use-llvm --system=linux-x86_64

	If using Android NDK r10e:
	<NDK_ROOT>/build/tools/make-standalone-toolchain.sh --ndk-dir=<NDK_ROOT> \
	--platform=android-21 --arch=arm64 --install-dir=<some_dir> \
	--toolchain=aarch64-linux-android-4.9 \
	--llvm-version=Snapdragon_LLVM_for_Android_3.8 --system=linux-x86_64

	The above command lines specify that android-21 platform directory must be
	copied to be used as the standalone sysroot and GCC-4.9 toolchain must be
	copied to be used as the linker and for other binutils such as objdump.


	12. By default, the ndk-build tool will build an application for all the valid
	targets (viz. ARMv7, AARCH64, MIPS, X86). This will result in compilation
	errors for MIPS and X86 targets since the Snapdragon LLVM ARM Compiler 3.8.8
	cannot generate code for these targets. The errors can be avoided if the user
	explicitly passes APP_ABI="armeabi-v7a" or APP_ABI="arm64-v8a" when using the
	Snapdragon LLVM ARM Compiler 3.8.8. However, in situations where the build
	system cannot be changed, and hence these flags cannot be set, we provide the
	following wrappers for MIPS and x86 in order to avoid compilation errors:

	mipsel-linux-android-snapdragonclang3.8
	mips64el-linux-android-snapdragonclang3.8
	x86-snapdragonclang3.8
	x86_64-snapdragonclang3.8

	These wrappers are exact copies of their Clang versions which are
	distributed as part of the Android NDK. These wrappers would simply invoke
	the LLVM compiler that comes with NDK (for MIPS and X86 targets only), thus
	avoiding compilation errors when using the Snapdragon LLVM ARM Compiler 3.8.

	Note: The Snapdragon LLVM ARM Compiler 3.8.8 would be invoked for ARMv7 and
	AARCH64 targets.

	Contacts & Bug Reporting
	http://developer.qualcomm.com/llvm-forum